import React, { Component } from 'react';
import './App.css'

import ImgUploader from  './components/Imguploder'
import PrintImg from './components/printImg'
import { BrowserRouter as Router, Route} from 'react-router-dom'

class App extends Component {

  



  render() {
  
    
    
    return (

      <Router>
      <div className="App">
          
        
          <Route exact path="/" render={() => <ImgUploader />} />
          <Route exact path="/PRINT" render={() => <PrintImg  />} />
      </div>
     
      </Router>
    );
  }
}

export default App;
