import React, { Component, Fragment } from "react";

import Cropper from "react-cropper";
import "./cropper.css";

class ImgUploader extends Component {
  state = {
    src: " ",
    fileName:" no file selected ",
    cropResult: null,
    displyImg: false,
    displayCropped: false,
    inputKey: "",
    alert: false,
    clearInput: false
  };

  //this function hnadle the change in the input element
  onChange = e => {
    e.preventDefault();
    this.setState({ displayCropped: false, clearInput: true, alert:false });

    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    
  
    const reader = new FileReader();
    reader.onload = () => {
      if ( (files[0].size < 1000000)){
      this.setState({
        src: reader.result,
        displyImg: true,
        fileName:files[0].name
      })}
      else{
        alert("maximum file size is 1 mb")
        return false
      }
    };
    if (files[0]){reader.readAsDataURL(files[0])}
    else{
      
  this.setState({fileName:"no file selected",
  clearInput:false
})};
  };

  //this function saves the url of cropped image to state and local storage
  cropImage = () => {
    if (typeof this.cropper.getCroppedCanvas() === "undefined") {
      return;
    } else {
      let size = this.cropper.getCroppedCanvas();
      if (size.width > 800) {
        this.setState({ alert: true });
        return false;
      } else {
        this.setState({
          cropResult: this.cropper.getCroppedCanvas().toDataURL(),
          displayCropped: true,
          displyImg: false,
          alert: false,


        });

        if (localStorage.getItem("items") === null) {
          const items = [];
          items.push(this.cropper.getCroppedCanvas().toDataURL());
          localStorage.setItem("items", JSON.stringify(items));
        } else {
          //recieves from localstorage
          const items = JSON.parse(localStorage.getItem("items"));
          items[0] = this.cropper.getCroppedCanvas().toDataURL();
          localStorage.setItem("items", JSON.stringify(items));
        }
      }
    }
  };
  //this function clears the input on button click
  clearInput = () => {
    //let randomString = Math.random().toString(36);
    this.fileInput.files=null;
 
     this.setState({
       fileName:" no file selected",
       alert:false,
       clearInput:false,
      displyImg: false,
      displayCropped: false
    })
      ;
  };

  // jsx rendering starts from here

  render() {



    return (
      // this section is rendered as image cropping area
      <Fragment>
        <h1 className="Heading"> Upload your image </h1>

        {this.state.displyImg ? (
          <div className ="imgCropper">
            <Cropper
              style={{
                height: "25vw", width: "100%",margin: "0 auto",marginBottom:"10px",
               border: "4px solid  #ffa502"
              }}
              aspectRatio={16 / 9}
              guides={false}
              src={this.state.src}
              ref={cropper => {
                this.cropper = cropper;
              }}
            />
            <button
              onClick={this.cropImage}
              className="crop"
              style={{ display: "block", margin: "0 auto", width: "50px" }}
            >
              Crop
            </button>
            
          </div>
          
        ) : null}

        {this.state.displayCropped ? <div
          className="box"
          style={{ margin: "0 auto", alignContent: "center" }}
        >

          <img
            style={{ maxWidth: "100%" }}
            src={this.state.cropResult}
            alt=" "
          />

          {this.state.displayCropped ? <div className="printIcon"> <a href="PRINT" target="_blank">
            <button> Save & preview </button>
          </a></div> : null}

        </div> : null}

        <div className="devider" > </div>

        <div className={"Inputs"}>
          <input
            accept="image/*"
            style={{ display: "none" }}
            className="myInput"
            type="file"
            ref={(fileInput) => this.fileInput = fileInput}

            onChange={this.onChange}
          />
            <p className='fileName'> {this.state.fileName}</p>

          <button
            onClick={() => { this.fileInput.click() }}
            className=" uploadButton myButton"> upload Image </button>
          {this.state.clearInput ? 
            <span onClick= {this.clearInput} ><i className="btn fas fa-times-circle"></i></span>  : null}


          {this.state.alert ? (
            <h4 className="alert">

              Maximum width is 800px ! please resize your cropper!
            </h4>
          ) : null}
        </div >

        
      </Fragment>
    );
  }
}
export default ImgUploader;
